class SuperHero:
    people = 'people'

    def __init__(self, name, nickname, superpower, health_points, catchphrase):
        self.name = name
        self.health_points = health_points
        self.nickname = nickname
        self.superpower = superpower
        self.catchphrase = catchphrase

    def get_name(self):
        return self.name

    def double_health(self):
        self.health_points *= 2
        return self.health_points
    def __str__(self):
        return f"###############################\n" \
               f"nickname is {self.nickname} \n" \
               f"Superpower of {self.superpower}\n" \
               f"Health_points = {self.health_points}\n " \
               f"###############################\n"

    def __len__(self):
        return len(self.catchphrase)

hero = SuperHero('Clark Kent','Superman','Super strength,speed,flight',500,"Looks like it's a job for Superman")
print(hero)
print(hero.get_name())
print(hero.double_health())
print(hero.__str__())
print(hero.__len__())

class AirHero(SuperHero):
    def __init__(self, name, nickname, superpower, health_points, catchphrase, damage, fly = True):
       super().__init__(name, nickname, superpower, health_points, catchphrase)
       self.damage = damage
       self.fly = fly

    def double_health_points(self):
        self.fly = True
        print(self.health_points ** 2)
    def fly_phrase(self):
        if self.fly_phrase():
            print('fly in the True phrase')

class EarthHero(SuperHero):
    def __init__(self, name, nickname, superpower, health_points, catchphrase, damage, fly = False):
       super().__init__(name, nickname, superpower, health_points, catchphrase)
       self.damage = damage
       self.fly = fly


    def double_health_points(self):
        self.fly = True
        return self.health_points ** 2
    def fly_phrase(self):
        if self.fly_phrase():
            print('fly in the True phrase')

class Villain(AirHero):
    people = 'monster'
    def gen_x(self):
        pass
    def crit(self,object2):
        if object2.damage:
            return object2.damage ** 2
villain = Villain('Loki Odinson', 'God of deception', 'illusions,flight,teleportation', 250, 'I am the god of deception', 75, True)

hero = SuperHero('Clark Kent','Superman','Super strength,speed,flight',500,"Looks like it's a job for Superman")
hero2 = AirHero('Tony Stark', 'Iron Man', 'flying,genius,money', 100, 'I am Iron Man', 35, True)
hero3 = EarthHero('Bruce Benner', 'Hulk', 'super stronger muscules', 300, "Don't make me angry, you won't like me",50,False)
print(villain.crit(hero2))
